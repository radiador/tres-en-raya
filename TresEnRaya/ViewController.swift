//
//  ViewController.swift
//  TresEnRaya
//
//  Created by cice on 27/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let gameBoardTag = 12
    let margin: CGFloat = 60
    
    let ticTacToeViewChecker = TicTacToeViewCell(withBackgroundColor: UIColor.orange, seletedBackgroundColor: UIColor.yellow, frame: CGRect(x: 10, y: 10, width: 100, height: 100))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myGameView = TicTacToeView()
        myGameView.tag = gameBoardTag
        myGameView.backgroundColor = UIColor.white
        myGameView.currentChecker = Checker.cross
        myGameView.delegate = self
        
        ticTacToeViewChecker.checker = myGameView.currentChecker
        
        view.addSubview(myGameView)
        view.addSubview(ticTacToeViewChecker)
        
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.green.cgColor]
        
        gradientLayer.frame = ticTacToeViewChecker.layer.bounds
        
        ticTacToeViewChecker.layer.addSublayer(gradientLayer)
        
    }

    override func viewWillLayoutSubviews() {
        
        guard let myGameView = view.viewWithTag(gameBoardTag) else {
            return
        }
        
        let sideSize = (view.bounds.width > view.bounds.height ? view.bounds.height : view.bounds.width) - (2 * margin)
        
        myGameView.frame = CGRect(x: margin, y: margin, width: sideSize, height: sideSize)
        

        let checkerSideSize = 50
        
        ticTacToeViewChecker.bounds = CGRect(x: 0, y: 0, width: checkerSideSize, height: checkerSideSize)
        ticTacToeViewChecker.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
        
    }
}

extension ViewController: TicTacToeViewDelegate {
    
    func modelCheckerChanged(checker: Checker) {
        ticTacToeViewChecker.checker = checker
        ticTacToeViewChecker.setNeedsDisplay()
    }
}
