//
//  TicTacToeView.swift
//  TresEnRaya
//
//  Created by cice on 27/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

struct TicTacToeViewModel: Codable {
    
    var cellsCkeckers = [Checker?]()
    var currentChecker: Checker?

    mutating func setupModel() {
        for _ in 0..<9 {
            cellsCkeckers.append(nil)
        }
    }
}

enum Checker: Int, Codable {
    
    case cross, circle
    
    func flipModel() -> Checker{
        if self == .cross {
            return .circle
        } else {
            return .cross
        }
    }
}

class TicTacToeViewCell: UIView {
    
    var checker: Checker?
    var seletedBackgroundColor = UIColor.lightGray
    var originalBackgroundcolor: UIColor?
    
    
    override convenience init(frame: CGRect) {
        self.init(withBackgroundColor: UIColor.white, frame: frame)
    }
    
    convenience init() {
        self.init(withBackgroundColor: UIColor.white, frame: CGRect.zero)
    }
    
    init(withBackgroundColor backgroundcolor: UIColor, seletedBackgroundColor: UIColor? = UIColor.lightGray, frame: CGRect) {

        super.init(frame:frame)

        originalBackgroundcolor = backgroundcolor
        self.backgroundColor = backgroundcolor
        self.seletedBackgroundColor = seletedBackgroundColor ?? UIColor.lightGray
        
        clearsContextBeforeDrawing = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let checker = checker else {
            
            UIColor.white.setFill()
            
            let rectPath = UIBezierPath(rect: rect)
            
            rectPath.fill()
            
            return }

        UIColor.red.setStroke()

        switch checker {
        case .circle:

            let oval = UIBezierPath(ovalIn: CGRect(x: 3, y: 3, width: bounds.width - 6, height: bounds.height - 6))

            oval.lineWidth = 3

            oval.stroke()

        case .cross:

            let cross = UIBezierPath()

            cross.lineCapStyle = .round

            cross.lineWidth = 3

            cross.move(to: CGPoint(x: 3, y: 3))
            cross.addLine(to: CGPoint(x: bounds.width - 3, y: bounds.height - 3))

            cross.move(to: CGPoint(x: 3, y: bounds.height - 3))
            cross.addLine(to: CGPoint(x: bounds.width - 3, y: 3))

            cross.stroke()

        }
        
    }
//        func setup() {
//
//            guard let checker = checker else { return }
//
//
//            switch checker {
//            case .circle:
//                let circleLayer = CAShapeLayer()
//
//                let oval = UIBezierPath(ovalIn: CGRect(x: 3, y: 3, width: bounds.width - 6, height: bounds.height - 6))
//
//                circleLayer.path = oval.cgPath
//
//                circleLayer.bounds = layer.frame
//                circleLayer.strokeColor = UIColor.red.cgColor
//
//                layer.addSublayer(circleLayer)
//
//            case .cross:
//
//                let crossLayer = CAShapeLayer()
//
//                let cross = UIBezierPath()
//
//                cross.lineCapStyle = .round
//
//                cross.lineWidth = 3
//
//                cross.move(to: CGPoint(x: 3, y: 3))
//                cross.addLine(to: CGPoint(x: bounds.width - 3, y: bounds.height - 3))
//
//                cross.move(to: CGPoint(x: 3, y: bounds.height - 3))
//                cross.addLine(to: CGPoint(x: bounds.width - 3, y: 3))
//                crossLayer.strokeColor = UIColor.red.cgColor
//
//                crossLayer.path = cross.cgPath
//
//                crossLayer.bounds = layer.frame
//
//                layer.addSublayer(crossLayer)
//            }
    
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        backgroundColor = seletedBackgroundColor
        
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        backgroundColor = originalBackgroundcolor
        
        super.touchesEnded(touches, with: event)
    }
}

protocol TicTacToeViewDelegate {
    func modelCheckerChanged(checker: Checker)
}

class TicTacToeView: UIView {
    
    var delegate: TicTacToeViewDelegate?
    let gameModelKey = "gameModelKey"

    var currentChecker: Checker?
    var model = TicTacToeViewModel()

    private let margin: CGFloat = 10
    
    private var cell11 = TicTacToeViewCell()
    private var cell21 = TicTacToeViewCell()
    private var cell31 = TicTacToeViewCell()
    private var cell12 = TicTacToeViewCell()
    private var cell22 = TicTacToeViewCell()
    private var cell32 = TicTacToeViewCell()
    private var cell13 = TicTacToeViewCell()
    private var cell23 = TicTacToeViewCell()
    private var cell33 = TicTacToeViewCell()
    
    private var checkerPositions = [Int]()
    
    init() {
        super.init(frame: CGRect.zero)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        let thirdWidth = bounds.width / 3
        let width = thirdWidth - (2 * margin)
        let thirdHeight = bounds.height / 3
        let height = thirdHeight - (2 * margin)
        
        cell11.frame = CGRect(x: posicionForIndex(1, size: width), y: posicionForIndex(1, size: height), width: width, height: height)
        cell12.frame = CGRect(x: posicionForIndex(2, size: width), y: posicionForIndex(1, size: height), width: width, height: height)
        cell13.frame = CGRect(x: posicionForIndex(3, size: width), y: posicionForIndex(1, size: height), width: width, height: height)
        cell21.frame = CGRect(x: posicionForIndex(1, size: width), y: posicionForIndex(2, size: height) , width: width, height: height)
        cell22.frame = CGRect(x: posicionForIndex(2, size: width), y: posicionForIndex(2, size: height) , width: width, height: height)
        cell23.frame = CGRect(x: posicionForIndex(3, size: width),y: posicionForIndex(2, size: height) , width: width, height: height)
        cell31.frame = CGRect(x: posicionForIndex(1, size: width), y: posicionForIndex(3, size: height) , width: width, height: height)
        cell32.frame = CGRect(x: posicionForIndex(2, size: width), y: posicionForIndex(3, size: height) , width: width, height: height)
        cell33.frame = CGRect(x: posicionForIndex(3, size: width), y: posicionForIndex(3, size: height) , width: width, height: height)
    }
    
    
    private func posicionForIndex(_ index: Int, size: CGFloat) -> CGFloat {
        
        switch index {
        case 1:
            return margin
        case 2:
            return (3 * margin) + size
        case 3:
            return (5 * margin) + (2 * size)
        default:
            return 0
        }
    }
    
    private func checkCheckers() {
        
        if checkerPositions.count > 6 {
            
            let tagLastAddedCheckerCell = checkerPositions[0]
            
            let lastAddedCheckerCell = viewWithTag(tagLastAddedCheckerCell) as? TicTacToeViewCell
            
            if let lastAddedCheckerCell = lastAddedCheckerCell {
                setCheckerForCellView(cell: lastAddedCheckerCell, checker: nil)
            }
            
            checkerPositions.remove(at: 0)
        }
        
        refreshViewFromModel()
    }
    
    private func setUp() {
        
        contentMode = .redraw
        
        configureCell(cell: cell11)
        configureCell(cell: cell12)
        configureCell(cell: cell13)
        configureCell(cell: cell21)
        configureCell(cell: cell22)
        configureCell(cell: cell23)
        configureCell(cell: cell31)
        configureCell(cell: cell32)
        configureCell(cell: cell33)
        
        cell11.tag = 11
        cell12.tag = 12
        cell13.tag = 13
        cell21.tag = 21
        cell22.tag = 22
        cell23.tag = 23
        cell31.tag = 31
        cell32.tag = 32
        cell33.tag = 33
        
        let modelSaved = UserDefaults.standard.value(forKey: gameModelKey)
        
        if let modelSavedJson = modelSaved as? Data{
            let modelSaved = try? JSONDecoder().decode(TicTacToeViewModel.self, from: modelSavedJson)
            
            if let modelSaved = modelSaved {
                model = modelSaved
                
                if let currentChecker = model.currentChecker {
                    delegate?.modelCheckerChanged(checker: currentChecker)
                }
            }
        } else {
            model.setupModel()
        }
        
        refreshViewFromModel()
        
        if let currentChecker = model.currentChecker {
            self.currentChecker = currentChecker
        }
        
        let checkerSubviews = subviews.filter { view -> Bool in
            return view is TicTacToeViewCell
        }
        
        for cell in checkerSubviews {
            if let cell = cell as? TicTacToeViewCell, cell.checker != nil {
                checkerPositions.append(cell.tag)
            }
        }
    }
    
    private func configureCell(cell: TicTacToeViewCell) {
        let layer = cell.layer
        
        layer.borderWidth = 5
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 5
        
        addSubview(cell)
        
        cell.contentMode = .redraw
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        let position = touch?.location(in: self)
        
        let viewTouched = hitTest(position!, with: nil)

        let subviewTouched = subviews.first { view -> Bool in
            return (view === viewTouched && view is TicTacToeViewCell)
        }
        
        if let subviewTouched = subviewTouched as? TicTacToeViewCell, let currentChecker = currentChecker {
            
            if subviewTouched.checker != nil {
                return
            }
            
           setCheckerForCellView(cell: subviewTouched, checker: currentChecker)
            
            checkerPositions.append(subviewTouched.tag)
            
            checkCheckers()
            
            self.currentChecker = currentChecker.flipModel()
            
            model.currentChecker = self.currentChecker
            
            
            let modelJson = try? JSONEncoder().encode(model)

            if let modelJson = modelJson {
                UserDefaults.standard.set(modelJson, forKey: gameModelKey)
                UserDefaults.standard.synchronize()
                
                if let currentChecker = self.currentChecker {

                    delegate?.modelCheckerChanged(checker: currentChecker)
                }
            }
        }
    }
    
    private func setCheckerForCellView(cell: TicTacToeViewCell, checker: Checker?) {
        
        switch cell.tag {
        case 11:
             model.cellsCkeckers[0] = checker
        case 12:
            model.cellsCkeckers[1] = checker
        case 13:
            model.cellsCkeckers[2] = checker
        case 21:
            model.cellsCkeckers[3] = checker
        case 22:
            model.cellsCkeckers[4] = checker
        case 23:
            model.cellsCkeckers[5] = checker
        case 31:
            model.cellsCkeckers[6] = checker
        case 32:
            model.cellsCkeckers[7] = checker
        case 33:
            model.cellsCkeckers[8] = checker
        default:
            return
        }
    }
    
    private func refreshViewFromModel() {
        
        cell11.checker = model.cellsCkeckers[0]
        cell12.checker = model.cellsCkeckers[1]
        cell13.checker = model.cellsCkeckers[2]
        cell21.checker = model.cellsCkeckers[3]
        cell22.checker = model.cellsCkeckers[4]
        cell23.checker = model.cellsCkeckers[5]
        cell31.checker = model.cellsCkeckers[6]
        cell32.checker = model.cellsCkeckers[7]
        cell33.checker = model.cellsCkeckers[8]
        
        setNeedsDisplay()
    }
    
}
